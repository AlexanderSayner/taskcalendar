#pragma once

#include "FCalendarPlate.generated.h"

/**
 * Represents each plate in the calendar grid
 * https://docs.unrealengine.com/5.2/en-US/using-structs-in-unreal-cpp/
 */
USTRUCT(BlueprintType)
struct FCalendarPlate
{
	GENERATED_BODY()

	//~ The following member variable will be accessible by Blueprint Graphs:
	// Day number for a plate
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 DayNumber;
	// Row in the calendar grid
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Row;
	// Day of week from 0 to 6 included
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 DayOfWeek;
	// true if this struct instance represents today's day 
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bIsToday;
};
