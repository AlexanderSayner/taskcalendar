// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CalendarMath.generated.h"

/**
 * Utility date functions class
 */
UCLASS()
class CALENDARUI_API UCalendarMath final : public UObject
{
	GENERATED_BODY()

public:
	// Returns day of week index
	static int32 DayOfWeekIndex(int32 Day, int32 Month, int32 Year);
	
private:
	// Returns enum
	static EDayOfWeek DayOfWeek(int32 Day, int32 Month, int32 Year);
	// Returns in range from 0 to 6
	static int32 EnumToDayOfWeekIndex(EDayOfWeek DayOfWeek);
};
