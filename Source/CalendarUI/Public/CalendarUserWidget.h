// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FCalendarPlate.h"
#include "Blueprint/UserWidget.h"
#include "CalendarUserWidget.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCalendar, Log, All);

/**
 * Parent class for blueprint widget provides specific cpp date functions
 * https://forums.unrealengine.com/t/create-widget-in-pure-c/367306
 * https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/GameplayArchitecture/Functions/
 */
UCLASS()
class CALENDARUI_API UCalendarUserWidget final : public UUserWidget
{
	GENERATED_BODY()

public:
	// 7 columns and 5 rows - classic calendar
	UFUNCTION(BlueprintCallable, BlueprintPure, Category="Calendar")
	static TArray<FCalendarPlate> GenerateGrid(int32 Year, int32 Month, int32 Today);

	// Opens file dialog for the platform
	UFUNCTION(BlueprintCallable, Category="CalendarFileSystem")
	static TArray<FString> OpenFileDialog(bool& bIsFileSelected);

	// Returns resolution of a texture
	UFUNCTION(BlueprintCallable, BlueprintPure, Category="Texture")
	static FVector2D GetTexture2DDynamicSize(UTexture2DDynamic* InTexture2DDynamicReference);

	// Month index from 1 to 12 included converts to enum
	UFUNCTION(BlueprintCallable, BlueprintPure, Category="Calendar|Helper")
	FString MonthName(int32 MonthIndex) const;
};
