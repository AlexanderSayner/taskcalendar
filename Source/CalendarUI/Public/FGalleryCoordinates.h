#pragma once

#include "FGalleryCoordinates.generated.h"

/**
 * Four dimension vector
 */
USTRUCT(BlueprintType)
struct FGalleryCoordinates
{
	GENERATED_BODY()

	// Image column
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 X;
	// Image row
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Y;
	// Image column span
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 U;
	// Image row span
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 W;
	// Image widget itself
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UUserWidget* GalleryImageWidget;
};
