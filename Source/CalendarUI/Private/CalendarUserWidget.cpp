// Fill out your copyright notice in the Description page of Project Settings.


#include "CalendarUserWidget.h"

#include "CalendarMath.h"
#include "DesktopPlatformModule.h"
#include "IDesktopPlatform.h"
#include "Engine/Texture2DDynamic.h"

DEFINE_LOG_CATEGORY(LogCalendar);

// 7 (0 to 6) columns and 5 (0 to 4) rows 
TArray<FCalendarPlate> UCalendarUserWidget::GenerateGrid(const int32 Year, const int32 Month, const int32 Today)
{
	TArray<FCalendarPlate> Plates;
	const int32 FirstWeekDay = UCalendarMath::DayOfWeekIndex(1, Month, Year);
	const int32 DaysInMonth = FDateTime::DaysInMonth(Year, Month);
	int32 DayNumberIteration = 1;
	for (int32 Row = 0; Row < 6; ++Row)
	{
		for (int32 Column = 0; Column < 7; ++Column)
		{
			FCalendarPlate Plate = FCalendarPlate();
			Plate.DayNumber = 0;
			Plate.Row = Row;
			Plate.DayOfWeek = Column;
			Plate.bIsToday = false;
			if (Row == 0 && Column < FirstWeekDay)
			{
				// Skip previous month days
			}
			else if (DayNumberIteration > DaysInMonth)
			{
				// Skip next month days
			}
			else
			{
				// This month day
				if (Today == DayNumberIteration)
				{
					Plate.bIsToday = true;
				}
				Plate.DayNumber = DayNumberIteration++;
			}
			Plates.Add(Plate);
		}
	}
	return Plates;
}

// Opens root folder of execution file to select png or jpg images
TArray<FString> UCalendarUserWidget::OpenFileDialog(bool& bIsFileSelected)
{
	void* WindowHandle = GEngine->GameViewport->GetWindow()->GetNativeWindow()->GetOSWindowHandle();
	FString DialogTitle = FString(TEXT("Cpp title"));
	//FString DefaultPath = FPaths::RootDir(); //FString(TEXT("/media/archon/Storage"));
	FString DefaultPath = FString(TEXT("/media/archon/Storage"));
	FString DefaultFile = FString(TEXT(""));
	FString FileTypes = FString(TEXT("PNG Image|*.png|JPG Image|*.jpg;*.jpeg|All files|*.*"));
	uint32 Flags = 0;
	TArray<FString> OutFilenames;
	IDesktopPlatform* DesktopPlatform = FDesktopPlatformModule::Get();
	bIsFileSelected = DesktopPlatform->OpenFileDialog(WindowHandle, DialogTitle, DefaultPath, DefaultFile,
	                                                  FileTypes,
	                                                  Flags, OutFilenames);
	return OutFilenames;
}

// X - Width; Y - Height
FVector2D UCalendarUserWidget::GetTexture2DDynamicSize(UTexture2DDynamic* InTexture2DDynamicReference)
{
	if (!InTexture2DDynamicReference)
	{
		return FVector2D();
	}
	return FVector2D(InTexture2DDynamicReference->SizeX, InTexture2DDynamicReference->SizeY);
}

// Uses https://docs.unrealengine.com/4.26/en-US/API/Runtime/Core/Misc/EMonthOfYear/
FString UCalendarUserWidget::MonthName(const int32 MonthIndex) const
{
	const int32 n = FMath::Clamp(MonthIndex,
	                             static_cast<int32>(EMonthOfYear::January),
	                             static_cast<int32>(EMonthOfYear::December));
	const EMonthOfYear MonthEnumValue = static_cast<EMonthOfYear>(n);
	switch (MonthEnumValue)
	{
	case EMonthOfYear::January:
		return FString(TEXT("January"));
	case EMonthOfYear::February:
		return FString(TEXT("February"));
	case EMonthOfYear::March:
		return FString(TEXT("March"));
	case EMonthOfYear::April:
		return FString(TEXT("April"));
	case EMonthOfYear::May:
		return FString(TEXT("May"));
	case EMonthOfYear::June:
		return FString(TEXT("June"));
	case EMonthOfYear::July:
		return FString(TEXT("July"));
	case EMonthOfYear::August:
		return FString(TEXT("August"));
	case EMonthOfYear::September:
		return FString(TEXT("September"));
	case EMonthOfYear::October:
		return FString(TEXT("October"));
	case EMonthOfYear::November:
		return FString(TEXT("November"));
	case EMonthOfYear::December:
		return FString(TEXT("December"));
	default:
		return FString(TEXT("Month"));
	}
}
