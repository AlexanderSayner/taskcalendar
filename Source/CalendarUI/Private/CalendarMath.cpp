// Fill out your copyright notice in the Description page of Project Settings.


#include "CalendarMath.h"

/*
 * Combination of private functions
 */
int32 UCalendarMath::DayOfWeekIndex(const int32 Day, const int32 Month, const int32 Year)
{
	return UCalendarMath::EnumToDayOfWeekIndex(UCalendarMath::DayOfWeek(Day, Month, Year));
}

/*
 * Gets day of a week Enum of required date
 */
EDayOfWeek UCalendarMath::DayOfWeek(const int32 Day, const int32 Month, const int32 Year)
{
	const FDateTime DateTime = FDateTime(Year, Month, Day);
	return DateTime.GetDayOfWeek();
}

/*
 * Converts enum to index
 */
int32 UCalendarMath::EnumToDayOfWeekIndex(const EDayOfWeek DayOfWeek)
{
	switch (DayOfWeek)
	{
	case EDayOfWeek::Monday:
		return 0;
	case EDayOfWeek::Tuesday:
		return 1;
	case EDayOfWeek::Wednesday:
		return 2;
	case EDayOfWeek::Thursday:
		return 3;
	case EDayOfWeek::Friday:
		return 4;
	case EDayOfWeek::Saturday:
		return 5;
	case EDayOfWeek::Sunday:
		return 6;
	default:
		return -1;
	}
}
